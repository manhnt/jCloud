import React from 'react';
import { Button, Form, Input, Grid, GridColumn, List } from 'semantic-ui-react';
import FileList from './FileList';
import { API_URL } from '../API.js';
import request from 'request';
import path from 'path';
import fs from 'fs';

class Files extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      files: []
    };
  }

  handleChange(e, { name, value }) {
    this.setState({ [name]: e.target.files });
  }

  handleSubmit() {
    const fileReader = new window.FileReader();
    const filename = this.state.files[0].name;

    fileReader.onload = (e) => {
      const options = {
        url: `${API_URL}/upload`,
        method: 'POST',
        json: true,
        headers: { 'Content-Type': 'multipart/form-data', 'token': `${sessionStorage.token}`},
        body: {},
        multipart: {
          chunked: false,
          data: [
          {
            'Content-Type': 'multipart/form-data',
            'Content-Disposition': `form-data; name="files"; filename="${filename}";`,
            body: e.target.result
          }
        ]
      }
    };

    request(options, (err, res, body) => {
      if (err) {
        console.log(err);
      } else {
        window.location.reload();
      }
    });};

    fileReader.readAsArrayBuffer(this.state.files[0]);
  }

  render() {
    return (
      <div>
        <Form onSubmit={() => { this.handleSubmit(); }}>
        <Grid columns={2}>
        <GridColumn width={11}>
          <Form.Input
            type="file" name="files"
            onChange={(e, { name, value }) => { this.handleChange(e, { name, value }); }}
          />
        </GridColumn>
        <GridColumn width={5}>
          <Button className="fluid ui button primary" size="big" type="submit">Upload</Button>
        </GridColumn>
        </Grid>
        </Form>

        <br />
        <FileList />
      </div>
      );
  }
}

export default Files;

