import React from 'react';
import { List } from 'semantic-ui-react';
import { API_URL } from '../API.js';
import request from 'request';

class FileList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      files: [] 
    };
  }

  componentWillMount() {
    const formData = {
      username: sessionStorage.username
    };

    const options = {
      url: `${API_URL}/files`,
      method: 'POST',
      headers: { 'token': `${sessionStorage.token}`},
      json: true,
      body: formData
    };

    request(options, (err, res, body) => {
      if (err) {
        console.log(err);
      } else {
        this.setState({ files: body });
      }
    });
  }

  render() {
    const files = this.state.files;
    const list = []
    for (var fileKey in files) {
      list.push(
        <List.Item>
          <List.Icon name="file" />
          <List.Content>
          <List.Header><a href={`${API_URL}/${files[fileKey].path}`} download>{files[fileKey].path}</a></List.Header>
          </List.Content>
        </List.Item>
      );
    }
    return (
      <List size="huge">
        {list}
      </List>
    );
  }
}

export default FileList;