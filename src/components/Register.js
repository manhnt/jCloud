import React from 'react';
import { browserHistory } from 'react-router';
import { Button, Form } from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import request from 'request';
import { API_URL } from '../API.js';

class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '', password: ''
    };
  }

  handleChange(e, { name, value }) {
    this.setState({ [name]: value });
  }

  handleSubmit() {
    const formData = this.state;

    const options = {
      url: `${API_URL}/register`,
      method: 'POST',
      json: true,
      body: formData
    };
    
    request(options, (err, res, body) => {
      if (err) {
        console.log(err);
      } else {
        if (body === "ok") {
          browserHistory.push('/#/login');
          location.reload();
        }
      }
    });
  }

  render() {
    return (
      <div className="form-input form-register column seven wide left aligned">
        <h2 className="title">Register</h2>
        <Form onSubmit={() => { this.handleSubmit(); }}>
          <div className="fields">
            <Form.Field>
              <label>Username</label>
              <Form.Input
                name="username" required
                onChange={(e, { name, value }) => { this.handleChange(e, { name, value }); }}
              />
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <Form.Input type="password"
                name="password" required
                onChange={(e, { name, value }) => { this.handleChange(e, { name, value }); }}
              />
            </Form.Field>
            <br />
          </div>
          <Button className="fluid ui button primary" type="submit">Submit</Button>
        </Form>
      </div>
    );
  }
}

export default Register;

