import React from 'react';
import { Button, Checkbox, Form } from 'semantic-ui-react';
import request from 'request';
import { API_URL } from '../API.js';
import { browserHistory } from 'react-router';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '', password: ''
    };
  }

  handleChange(e, { name, value }) {
    this.setState({ [name]: value });
  }

  handleSubmit() {
    const formData = {
      username: this.state.username,
      password: this.state.password
    };

    const options = {
      url: `${API_URL}/login`,
      method: 'POST',
      json: true,
      body: formData
    };

    request(options, (err, res, body) => {
      if (err) {
        console.log(err);
      } else if (body.token.length) {
        sessionStorage.loggedIn = true;
        sessionStorage.token = body.token;
        sessionStorage.username = formData.username;
        browserHistory.push('/#/files');
        location.reload();
      }
    });
  }

  render() {
    return (
      <div className="form-input form-login column eight wide">
        <h2 className="title">Login</h2>
        <Form onSubmit={() => { this.handleSubmit(); }}>
          <Form.Field>
            <Form.Input
              name="username" placeholder="Username" required
              onChange={(e, { name, value }) => { this.handleChange(e, { name, value }); }}
            />
          </Form.Field>
          <Form.Field>
            <Form.Input
              name="password" placeholder="Password" type="password" required
              onChange={(e, { name, value }) => { this.handleChange(e, { name, value }); }}
            />
          </Form.Field>
          <Form.Field>
            <Checkbox label="Remember me" />
          </Form.Field>
          <Button className="fluid ui button primary" type="submit">Submit</Button>
        </Form>
      </div>
    );
  }
}

export default Login;

